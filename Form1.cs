﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using StreamHelper;

namespace HideData
{
    public partial class Form1 : Form
    {
        String srcFilePath;
        String distFilePath;
        String textToHide;
        String fileToHide;
        String fileToShow;
        HideInfos hideInfos;

        public Form1()
        {
            InitializeComponent();
        }

        private void brwsSrcToHideInBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image Files(*.bmp;*.jpg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png|Video File (*.avi)|*avi";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                srcFilePath = openFileDialog1.FileName;
                srcFileToHideTxt.Text = srcFilePath;
            }
        }

        private void savDistHideBtn_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Image Files(*.png)|*.png|Video File (*.avi)|*avi";
            saveFileDialog1.FileName = "";
            if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                distFilePath = saveFileDialog1.FileName;
                savDisHideTxt.Text = distFilePath;
            }

        }

        private void hideInfoBtn_Click(object sender, EventArgs e)
        {
            if (srcFileToHideTxt.Text != "")
            {
                if (savDisHideTxt.Text != "")
                {
                    if (hideTxtRdo.Checked)
                    {
                        if (toHideTxt.Text != "")
                        {
                            textToHide = toHideTxt.Text;
                            hideInfos = new HideInfos(srcFilePath);

                            BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());
                            messageWriter.Write((long)textToHide.Length);
                            messageWriter.Write(Encoding.ASCII.GetBytes(textToHide));
                            messageWriter.Seek(0, SeekOrigin.Begin);
                            if (srcFilePath.Split('.').Last().ToLower() == "avi")
                                hideInfos.embedInVideo(messageWriter.BaseStream, distFilePath);
                            else
                                hideInfos.embedInImage(messageWriter.BaseStream, distFilePath);
                        }
                        else MessageBox.Show("Enter text to hide it", "Error!");
                    }
                    else if (hideFileRdo.Checked)
                    {
                        if (fileToHideTxt.Text != "")
                        {
                            hideInfos = new HideInfos(srcFilePath);
                            BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());
                            FileStream fileStream = new FileStream(fileToHide, FileMode.Open);
                            long fileLength = fileStream.Length;
                            messageWriter.Write(fileLength);
                            byte[] buffer = new byte[fileStream.Length];
                            buffer = fileStream.ReadToEnd();
                            messageWriter.Write(buffer);
                            fileStream.Close();
                            messageWriter.Seek(0, SeekOrigin.Begin);
                            if (srcFilePath.Split('.').Last().ToLower() == "avi")
                                hideInfos.embedInVideo(messageWriter.BaseStream, distFilePath);
                            else
                                hideInfos.embedInImage(messageWriter.BaseStream, distFilePath);
                        }
                        else MessageBox.Show("Enter file to hide it", "Error!");
                    }
                }
                else MessageBox.Show("Enter Distenation file to save it", "Error!");
            }
            else MessageBox.Show("Enter Source file to hide in", "Error!");
        }

        private void brwsSrcToShowFromBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image Files(*.png)|*.png|Video File (*.avi)|*avi";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK) 
            {
                srcFilePath = openFileDialog1.FileName;
                srcFileToShowTxt.Text = srcFilePath;
            }
        }

        private void showHiddenDataBtn_Click(object sender, EventArgs e)
        {
            if (srcFileToShowTxt.Text != "")
            {
                if (showToTxtRdo.Checked)
                {
                    HideInfos showInfo = new HideInfos(srcFilePath);
                    Stream stream;
                    if (srcFilePath.Split('.').Last().ToLower() == "avi")
                    {
                        stream = showInfo.extractFromVideo();
                    }
                    else
                    {
                        stream = showInfo.extractFromImage();
                    }
                    byte[] tmp = stream.ReadToEnd();
                    hiddenTxt.Text = Encoding.ASCII.GetString(tmp);
                }
                else if (showInFileRdo.Checked)
                {
                    HideInfos showInfo = new HideInfos(srcFilePath);
                    Stream stream;
                    if (srcFilePath.Split('.').Last().ToLower() == "avi")
                    {
                        stream = showInfo.extractFromVideo();
                    }
                    else
                    {
                        stream = showInfo.extractFromImage();
                    }
                    FileStream fs = new FileStream(fileToShow, FileMode.Create);
                    byte[] buffer = stream.ReadToEnd();
                    fs.Write(buffer, 0, buffer.Length);
                    fs.Close();
                }
            }
            else MessageBox.Show("Enter source file to show hidden data", "Error!");
        }

        private void showInFileRdo_Click(object sender, EventArgs e)
        {
            savDistShowBtn.Enabled = true;
            fileToShowTxt.BackColor = Color.FromArgb(255, 255, 255);
            hiddenTxt.Enabled = false;
        }

        private void showToTxtRdo_Click(object sender, EventArgs e)
        {
            hiddenTxt.Enabled = true;
            savDistShowBtn.Enabled = false;
            fileToShowTxt.BackColor = Color.LightGray;
        }

        private void hideFileRdo_Click(object sender, EventArgs e)
        {
            fileToHideTxt.BackColor = Color.FromArgb(255, 255, 255);
            brwsFileToHideBtn.Enabled = true;
            toHideTxt.Enabled = false;
        }

        private void hideTxtRdo_Click(object sender, EventArgs e)
        {
            toHideTxt.Enabled = true;
            fileToHideTxt.BackColor = Color.LightGray;
            brwsFileToHideBtn.Enabled = false;
        }

        private void brwsFileToHideBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileToHide = openFileDialog1.FileName;
                fileToHideTxt.Text = fileToHide;
            }
        }

        private void savDistShowBtn_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "All files (*.*)|*.*";
            saveFileDialog1.FileName = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileToShow = saveFileDialog1.FileName;
                fileToShowTxt.Text = fileToShow;
            }
        }
    }
}
