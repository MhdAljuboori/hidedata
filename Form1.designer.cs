﻿namespace HideData
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTab = new System.Windows.Forms.TabControl();
            this.hideTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hideInfoBtn = new System.Windows.Forms.Button();
            this.brwsFileToHideBtn = new System.Windows.Forms.Button();
            this.toHideTxt = new System.Windows.Forms.TextBox();
            this.fileToHideTxt = new System.Windows.Forms.TextBox();
            this.hideTxtRdo = new System.Windows.Forms.RadioButton();
            this.hideFileRdo = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.savDisHideTxt = new System.Windows.Forms.TextBox();
            this.savDistHideBtn = new System.Windows.Forms.Button();
            this.brwsSrcToHideInBtn = new System.Windows.Forms.Button();
            this.srcFileToHideTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.showTab = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.showHiddenDataBtn = new System.Windows.Forms.Button();
            this.savDistShowBtn = new System.Windows.Forms.Button();
            this.hiddenTxt = new System.Windows.Forms.TextBox();
            this.fileToShowTxt = new System.Windows.Forms.TextBox();
            this.showToTxtRdo = new System.Windows.Forms.RadioButton();
            this.showInFileRdo = new System.Windows.Forms.RadioButton();
            this.brwsSrcToShowFromBtn = new System.Windows.Forms.Button();
            this.srcFileToShowTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.mainTab.SuspendLayout();
            this.hideTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.showTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.hideTab);
            this.mainTab.Controls.Add(this.showTab);
            this.mainTab.Location = new System.Drawing.Point(0, 2);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 0;
            this.mainTab.Size = new System.Drawing.Size(539, 297);
            this.mainTab.TabIndex = 0;
            // 
            // hideTab
            // 
            this.hideTab.Controls.Add(this.groupBox1);
            this.hideTab.Controls.Add(this.label2);
            this.hideTab.Controls.Add(this.savDisHideTxt);
            this.hideTab.Controls.Add(this.savDistHideBtn);
            this.hideTab.Controls.Add(this.brwsSrcToHideInBtn);
            this.hideTab.Controls.Add(this.srcFileToHideTxt);
            this.hideTab.Controls.Add(this.label1);
            this.hideTab.Location = new System.Drawing.Point(4, 22);
            this.hideTab.Name = "hideTab";
            this.hideTab.Padding = new System.Windows.Forms.Padding(3);
            this.hideTab.Size = new System.Drawing.Size(531, 271);
            this.hideTab.TabIndex = 0;
            this.hideTab.Text = "Hide";
            this.hideTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hideInfoBtn);
            this.groupBox1.Controls.Add(this.brwsFileToHideBtn);
            this.groupBox1.Controls.Add(this.toHideTxt);
            this.groupBox1.Controls.Add(this.fileToHideTxt);
            this.groupBox1.Controls.Add(this.hideTxtRdo);
            this.groupBox1.Controls.Add(this.hideFileRdo);
            this.groupBox1.Location = new System.Drawing.Point(8, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 173);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Information To Hide";
            // 
            // hideInfoBtn
            // 
            this.hideInfoBtn.Font = new System.Drawing.Font("Square721 BT", 17F, System.Drawing.FontStyle.Bold);
            this.hideInfoBtn.Location = new System.Drawing.Point(416, 133);
            this.hideInfoBtn.Name = "hideInfoBtn";
            this.hideInfoBtn.Size = new System.Drawing.Size(75, 34);
            this.hideInfoBtn.TabIndex = 8;
            this.hideInfoBtn.Text = "Hide";
            this.hideInfoBtn.UseVisualStyleBackColor = true;
            this.hideInfoBtn.Click += new System.EventHandler(this.hideInfoBtn_Click);
            // 
            // brwsFileToHideBtn
            // 
            this.brwsFileToHideBtn.Enabled = false;
            this.brwsFileToHideBtn.Location = new System.Drawing.Point(416, 27);
            this.brwsFileToHideBtn.Name = "brwsFileToHideBtn";
            this.brwsFileToHideBtn.Size = new System.Drawing.Size(75, 23);
            this.brwsFileToHideBtn.TabIndex = 7;
            this.brwsFileToHideBtn.Text = "Browse";
            this.brwsFileToHideBtn.UseVisualStyleBackColor = true;
            this.brwsFileToHideBtn.Click += new System.EventHandler(this.brwsFileToHideBtn_Click);
            // 
            // toHideTxt
            // 
            this.toHideTxt.Location = new System.Drawing.Point(98, 64);
            this.toHideTxt.Multiline = true;
            this.toHideTxt.Name = "toHideTxt";
            this.toHideTxt.Size = new System.Drawing.Size(393, 63);
            this.toHideTxt.TabIndex = 3;
            // 
            // fileToHideTxt
            // 
            this.fileToHideTxt.Enabled = false;
            this.fileToHideTxt.Location = new System.Drawing.Point(98, 29);
            this.fileToHideTxt.Name = "fileToHideTxt";
            this.fileToHideTxt.Size = new System.Drawing.Size(295, 20);
            this.fileToHideTxt.TabIndex = 2;
            // 
            // hideTxtRdo
            // 
            this.hideTxtRdo.AutoSize = true;
            this.hideTxtRdo.Checked = true;
            this.hideTxtRdo.Location = new System.Drawing.Point(21, 65);
            this.hideTxtRdo.Name = "hideTxtRdo";
            this.hideTxtRdo.Size = new System.Drawing.Size(71, 17);
            this.hideTxtRdo.TabIndex = 1;
            this.hideTxtRdo.TabStop = true;
            this.hideTxtRdo.Text = "Hide Text";
            this.hideTxtRdo.UseVisualStyleBackColor = true;
            this.hideTxtRdo.Click += new System.EventHandler(this.hideTxtRdo_Click);
            // 
            // hideFileRdo
            // 
            this.hideFileRdo.AutoSize = true;
            this.hideFileRdo.Location = new System.Drawing.Point(21, 30);
            this.hideFileRdo.Name = "hideFileRdo";
            this.hideFileRdo.Size = new System.Drawing.Size(65, 17);
            this.hideFileRdo.TabIndex = 0;
            this.hideFileRdo.Text = "Hide File";
            this.hideFileRdo.UseVisualStyleBackColor = true;
            this.hideFileRdo.Click += new System.EventHandler(this.hideFileRdo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Save File";
            // 
            // savDisHideTxt
            // 
            this.savDisHideTxt.BackColor = System.Drawing.Color.White;
            this.savDisHideTxt.Enabled = false;
            this.savDisHideTxt.ForeColor = System.Drawing.Color.Black;
            this.savDisHideTxt.Location = new System.Drawing.Point(104, 54);
            this.savDisHideTxt.Name = "savDisHideTxt";
            this.savDisHideTxt.Size = new System.Drawing.Size(297, 20);
            this.savDisHideTxt.TabIndex = 4;
            // 
            // savDistHideBtn
            // 
            this.savDistHideBtn.Location = new System.Drawing.Point(424, 52);
            this.savDistHideBtn.Name = "savDistHideBtn";
            this.savDistHideBtn.Size = new System.Drawing.Size(75, 23);
            this.savDistHideBtn.TabIndex = 3;
            this.savDistHideBtn.Text = "Save";
            this.savDistHideBtn.UseVisualStyleBackColor = true;
            this.savDistHideBtn.Click += new System.EventHandler(this.savDistHideBtn_Click);
            // 
            // brwsSrcToHideInBtn
            // 
            this.brwsSrcToHideInBtn.Location = new System.Drawing.Point(424, 11);
            this.brwsSrcToHideInBtn.Name = "brwsSrcToHideInBtn";
            this.brwsSrcToHideInBtn.Size = new System.Drawing.Size(75, 23);
            this.brwsSrcToHideInBtn.TabIndex = 2;
            this.brwsSrcToHideInBtn.Text = "Browse";
            this.brwsSrcToHideInBtn.UseVisualStyleBackColor = true;
            this.brwsSrcToHideInBtn.Click += new System.EventHandler(this.brwsSrcToHideInBtn_Click);
            // 
            // srcFileToHideTxt
            // 
            this.srcFileToHideTxt.BackColor = System.Drawing.Color.White;
            this.srcFileToHideTxt.Enabled = false;
            this.srcFileToHideTxt.ForeColor = System.Drawing.Color.Black;
            this.srcFileToHideTxt.Location = new System.Drawing.Point(104, 13);
            this.srcFileToHideTxt.Name = "srcFileToHideTxt";
            this.srcFileToHideTxt.Size = new System.Drawing.Size(297, 20);
            this.srcFileToHideTxt.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source File";
            // 
            // showTab
            // 
            this.showTab.Controls.Add(this.groupBox2);
            this.showTab.Controls.Add(this.brwsSrcToShowFromBtn);
            this.showTab.Controls.Add(this.srcFileToShowTxt);
            this.showTab.Controls.Add(this.label3);
            this.showTab.Location = new System.Drawing.Point(4, 22);
            this.showTab.Name = "showTab";
            this.showTab.Padding = new System.Windows.Forms.Padding(3);
            this.showTab.Size = new System.Drawing.Size(531, 271);
            this.showTab.TabIndex = 1;
            this.showTab.Text = "Show";
            this.showTab.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.showHiddenDataBtn);
            this.groupBox2.Controls.Add(this.savDistShowBtn);
            this.groupBox2.Controls.Add(this.hiddenTxt);
            this.groupBox2.Controls.Add(this.fileToShowTxt);
            this.groupBox2.Controls.Add(this.showToTxtRdo);
            this.groupBox2.Controls.Add(this.showInFileRdo);
            this.groupBox2.Location = new System.Drawing.Point(8, 91);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(514, 173);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Information To Hide";
            // 
            // showHiddenDataBtn
            // 
            this.showHiddenDataBtn.Font = new System.Drawing.Font("Square721 BT", 17F, System.Drawing.FontStyle.Bold);
            this.showHiddenDataBtn.Location = new System.Drawing.Point(404, 133);
            this.showHiddenDataBtn.Name = "showHiddenDataBtn";
            this.showHiddenDataBtn.Size = new System.Drawing.Size(87, 34);
            this.showHiddenDataBtn.TabIndex = 8;
            this.showHiddenDataBtn.Text = "Show";
            this.showHiddenDataBtn.UseVisualStyleBackColor = true;
            this.showHiddenDataBtn.Click += new System.EventHandler(this.showHiddenDataBtn_Click);
            // 
            // savDistShowBtn
            // 
            this.savDistShowBtn.Enabled = false;
            this.savDistShowBtn.Location = new System.Drawing.Point(416, 27);
            this.savDistShowBtn.Name = "savDistShowBtn";
            this.savDistShowBtn.Size = new System.Drawing.Size(75, 23);
            this.savDistShowBtn.TabIndex = 7;
            this.savDistShowBtn.Text = "Save";
            this.savDistShowBtn.UseVisualStyleBackColor = true;
            this.savDistShowBtn.Click += new System.EventHandler(this.savDistShowBtn_Click);
            // 
            // hiddenTxt
            // 
            this.hiddenTxt.Location = new System.Drawing.Point(98, 64);
            this.hiddenTxt.Multiline = true;
            this.hiddenTxt.Name = "hiddenTxt";
            this.hiddenTxt.Size = new System.Drawing.Size(393, 63);
            this.hiddenTxt.TabIndex = 3;
            // 
            // fileToShowTxt
            // 
            this.fileToShowTxt.Enabled = false;
            this.fileToShowTxt.Location = new System.Drawing.Point(98, 29);
            this.fileToShowTxt.Name = "fileToShowTxt";
            this.fileToShowTxt.Size = new System.Drawing.Size(295, 20);
            this.fileToShowTxt.TabIndex = 2;
            // 
            // showToTxtRdo
            // 
            this.showToTxtRdo.AutoSize = true;
            this.showToTxtRdo.Checked = true;
            this.showToTxtRdo.Location = new System.Drawing.Point(21, 65);
            this.showToTxtRdo.Name = "showToTxtRdo";
            this.showToTxtRdo.Size = new System.Drawing.Size(76, 17);
            this.showToTxtRdo.TabIndex = 1;
            this.showToTxtRdo.TabStop = true;
            this.showToTxtRdo.Text = "Show Text";
            this.showToTxtRdo.UseVisualStyleBackColor = true;
            this.showToTxtRdo.Click += new System.EventHandler(this.showToTxtRdo_Click);
            // 
            // showInFileRdo
            // 
            this.showInFileRdo.AutoSize = true;
            this.showInFileRdo.Location = new System.Drawing.Point(21, 30);
            this.showInFileRdo.Name = "showInFileRdo";
            this.showInFileRdo.Size = new System.Drawing.Size(70, 17);
            this.showInFileRdo.TabIndex = 0;
            this.showInFileRdo.Text = "Show File";
            this.showInFileRdo.UseVisualStyleBackColor = true;
            this.showInFileRdo.Click += new System.EventHandler(this.showInFileRdo_Click);
            // 
            // brwsSrcToShowFromBtn
            // 
            this.brwsSrcToShowFromBtn.Location = new System.Drawing.Point(422, 22);
            this.brwsSrcToShowFromBtn.Name = "brwsSrcToShowFromBtn";
            this.brwsSrcToShowFromBtn.Size = new System.Drawing.Size(75, 23);
            this.brwsSrcToShowFromBtn.TabIndex = 5;
            this.brwsSrcToShowFromBtn.Text = "Browse";
            this.brwsSrcToShowFromBtn.UseVisualStyleBackColor = true;
            this.brwsSrcToShowFromBtn.Click += new System.EventHandler(this.brwsSrcToShowFromBtn_Click);
            // 
            // srcFileToShowTxt
            // 
            this.srcFileToShowTxt.BackColor = System.Drawing.Color.White;
            this.srcFileToShowTxt.Enabled = false;
            this.srcFileToShowTxt.ForeColor = System.Drawing.Color.Black;
            this.srcFileToShowTxt.Location = new System.Drawing.Point(102, 24);
            this.srcFileToShowTxt.Name = "srcFileToShowTxt";
            this.srcFileToShowTxt.Size = new System.Drawing.Size(297, 20);
            this.srcFileToShowTxt.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Source File";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 300);
            this.Controls.Add(this.mainTab);
            this.Name = "Form1";
            this.Text = "HideInfos";
            this.mainTab.ResumeLayout(false);
            this.hideTab.ResumeLayout(false);
            this.hideTab.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.showTab.ResumeLayout(false);
            this.showTab.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.TabPage hideTab;
        private System.Windows.Forms.TabPage showTab;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button brwsFileToHideBtn;
        private System.Windows.Forms.TextBox toHideTxt;
        private System.Windows.Forms.TextBox fileToHideTxt;
        private System.Windows.Forms.RadioButton hideTxtRdo;
        private System.Windows.Forms.RadioButton hideFileRdo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox savDisHideTxt;
        private System.Windows.Forms.Button savDistHideBtn;
        private System.Windows.Forms.Button brwsSrcToHideInBtn;
        private System.Windows.Forms.TextBox srcFileToHideTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button hideInfoBtn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button brwsSrcToShowFromBtn;
        private System.Windows.Forms.TextBox srcFileToShowTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button showHiddenDataBtn;
        private System.Windows.Forms.Button savDistShowBtn;
        private System.Windows.Forms.TextBox hiddenTxt;
        private System.Windows.Forms.TextBox fileToShowTxt;
        private System.Windows.Forms.RadioButton showToTxtRdo;
        private System.Windows.Forms.RadioButton showInFileRdo;
    }
}

