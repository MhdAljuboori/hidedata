﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
using AForge.Video.FFMPEG;
using AForge.Video.VFW;
using System.IO;
using System.Drawing.Imaging;

namespace HideData
{
    class HideInfos
    {
        Bitmap sourceImage, videoFrame;
        VideoFileReader reader;
        AVIWriter writer;

        String sourceFilePath, imageCapacityBits, videoCapacityBits;

        public HideInfos(String fileName)
        {
            sourceFilePath = fileName;
        }

        // Change LSB of byte: b by value: value
        private byte getLSBChangedByte(byte b, bool value) 
        {
            BitArray bitsOfB = new BitArray(new byte[] { b });
            bitsOfB.Set(0, value);
            byte[] bytes = new byte[1];
            bitsOfB.CopyTo(bytes, 0);
            return bytes[0];
        }

        // Convert bit array to byte
        private byte ConvertFromBitArrayToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

        // Change color by adding informations from txtBits
        private Color getChangedColor(Color pixel, ref int charIndex, ref int txtIndex, String bitsText, bool isData)
        {
            byte redByte = pixel.R, greenByte = pixel.G, blueByte = pixel.B;
            redByte = getLSBChangedByte(redByte, (bitsText[txtIndex] == '1'));
            if (txtIndex + 1 < bitsText.Length)
                greenByte = getLSBChangedByte(greenByte, (bitsText[txtIndex + 1] == '1'));
            if (txtIndex + 2 < bitsText.Length && (charIndex < 6 || !isData))
                blueByte = getLSBChangedByte(blueByte, (bitsText[txtIndex + 2] == '1'));
            else
            { charIndex = -2; txtIndex -= 1; }

            // Return changed color
            return Color.FromArgb(redByte, greenByte, blueByte);
        }

        // Full image with data
        private bool fullWithImage(ref int rowIndex, ref int colIndex, String bitsText, bool isData)
        {
            for (int i = 0, j=1; i < bitsText.Length; i += 3, j += 3)
            {
                if (colIndex >= sourceImage.Height)
                {
                    rowIndex++;
                    colIndex = 0;
                }

                Color pixel = sourceImage.GetPixel(rowIndex, colIndex);
                sourceImage.SetPixel(rowIndex, colIndex, 
                    getChangedColor(pixel, ref j, ref i, bitsText, isData));
                colIndex++;
            }
            return true;
        }

        // Full video with data
        private bool fullWithVideo(ref int rowIndex, ref int colIndex, ref long frameIndex, String bitsText, bool isData)
        {
            for (int i = 0, j = 1; i < bitsText.Length; i += 3, j += 3)
            {
                if (colIndex >= videoFrame.Height)
                {
                    rowIndex++;
                    colIndex = 0;
                    if (rowIndex >= videoFrame.Width)
                    {
                        rowIndex = 0;
                        frameIndex++;
                        writer.AddFrame(videoFrame);
                        videoFrame = reader.ReadVideoFrame();
                    }
                }
                Color pixel = videoFrame.GetPixel(rowIndex, colIndex);
                videoFrame.SetPixel(rowIndex, colIndex, 
                    getChangedColor(pixel, ref j, ref i, bitsText, isData));
                colIndex++;
            }
            return true;
        }

        // Caculate image capacity
        private void calculateImageCapacity()
        {
            // Number of pixel
            long imageSize = sourceImage.Width * sourceImage.Height;
            // Number of letters can hide in image
            // Use R, G and B
            long capacity = (long)Math.Floor(imageSize / (double)3);
            // Bit needed for capacity
            imageCapacityBits = Convert.ToString(capacity, 2);
        }

        // Caculate video capacity
        private void calculateVideoCapacity()
        {
            // Number of letters can hide in image
            // Use R, G and B
            long videoCapacity = (reader.Width * reader.Height * reader.FrameCount) / 3;
            // Bit needed for capacity
            videoCapacityBits = Convert.ToString(videoCapacity, 2);
        }

        // return true if data smaller than or equal to data can hide in image
        // return false otherwise
        private bool checkIfImageCanHide(Stream stream)
        {
            calculateImageCapacity();

            // Get stream length (number of byte)
            //      Length - 8 becuase the first 8 elements is the length of text(long)
            long bitsSize = stream.Length-8;
            
            // Size of data can hidden
            long dataSize = sourceImage.Width * sourceImage.Height - (long)Math.Ceiling((double)(imageCapacityBits.Length / (double)3));

            if (Math.Floor(dataSize/(double)3) >= bitsSize)
                return true;
            else
                return false;
        }

        // return true if data smaller than or equal to data can hide in video
        // return false otherwise
        private bool checkIfVideoCanHide(Stream stream)
        {
            calculateVideoCapacity();

            // Get stream length (number of byte)
            //      Length - 8 becuase the first 8 elements is the length of text(long)
            long bitsSize = stream.Length - 8;

            // Size of data can hidden
            long dataSize = reader.Width * reader.Height * reader.FrameCount - (long)Math.Ceiling((double)(videoCapacityBits.Length / (double)3));

            if (Math.Floor(dataSize / (double)3) >= bitsSize)
                return true;
            else
                return false;
        }

        // Get LSB Bit
        private char getLSBBit(byte b)
        {
            String bits = Convert.ToString(Int32.Parse(b.ToString()), 2);
            return bits[bits.Length-1];
        }

        // Embed stream in image
        public bool embedInImage(Stream stream, String distFileName)
        {
            sourceImage = (Bitmap)Image.FromFile(sourceFilePath);

            if (!checkIfImageCanHide(stream))
                return false;

            byte[] tmp = new byte[8];
            stream.Read(tmp, 0, 8);
            String textLengthBits = Convert.ToString(BitConverter.ToInt32(tmp, 0), 2).PadLeft(imageCapacityBits.Length, '0');

            int textBuffer;
            String textBits = String.Empty;
            while ((textBuffer = stream.ReadByte()) >= 0)
            {
                textBits += Convert.ToString(textBuffer, 2).PadLeft(8, '0');
            }

            int rowIndex = 0, colIndex = 0;
            fullWithImage(ref rowIndex, ref colIndex, textLengthBits, false);
            fullWithImage(ref rowIndex, ref colIndex, textBits, true);
            sourceImage.Save(distFileName, ImageFormat.Png);

            return true;
        }

        // Extract data from image
        public Stream extractFromImage()
        {
            sourceImage = new Bitmap(sourceFilePath);
            BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());

            calculateImageCapacity();

            int rowIndex = 0, colIndex = 0;
            int capacityBitsLength = imageCapacityBits.Length;

            // Get Length of Text hidden in image
            String textLengthBits = String.Empty;
            for (int i = 0; i < capacityBitsLength; i += 3)
            {
                if (colIndex >= sourceImage.Height)
                {
                    rowIndex++;
                    colIndex = 0;
                }

                // Get pixel color
                Color pixel = sourceImage.GetPixel(rowIndex, colIndex);
                byte redByte = pixel.R, greenByte = pixel.G, blueByte = pixel.B;
                // Get bits from colors
                textLengthBits += getLSBBit(redByte);
                if (i + 1 < capacityBitsLength)
                    textLengthBits += getLSBBit(greenByte);
                if (i + 2 < capacityBitsLength)
                    textLengthBits += getLSBBit(blueByte);
                // Go to next pixel
                colIndex++;
            }
            // Convert bits to number (Length of text)
            int textLength = Convert.ToInt32(textLengthBits, 2);

            if (textLength > Convert.ToInt32(imageCapacityBits, 2))
                return messageWriter.BaseStream;

            byte[] textByte = new byte[textLength];

            // Get hidden text
            for (int i = 0; i < textLength * 8; i += 8)
            {
                BitArray textBits = new BitArray(8);
                // Get a Letter (character)
                for (int k = 7; k >= 0; k -= 3)
                {
                    if (colIndex >= sourceImage.Height)
                    {
                        rowIndex++;
                        colIndex = 0;
                    }

                    Color pixel = sourceImage.GetPixel(rowIndex, colIndex);
                    byte redByte = pixel.R, greenByte = pixel.G, blueByte = pixel.B;
                    textBits[k] = (getLSBBit(redByte) == '1');
                    textBits[k - 1] = (getLSBBit(greenByte) == '1');
                    if (k != 1)
                        textBits[k - 2] = (getLSBBit(blueByte) == '1');

                    // Go to next pixel
                    colIndex++;
                }

                messageWriter.Write(ConvertFromBitArrayToByte(textBits));
            }

            // Return the Stream
            messageWriter.Seek(0, SeekOrigin.Begin);
            return messageWriter.BaseStream;
        }

        // Embed stream in video
        public bool embedInVideo(Stream stream, String distFileName)
        {
            reader = new VideoFileReader();
            reader.Open(sourceFilePath);

            // chech if can hide
            if (!checkIfVideoCanHide(stream))
                return false;

            writer = new AVIWriter();
            writer.Open(distFileName, reader.Width, reader.Height);

            long frameCount = reader.FrameCount;
            bool finishHiding = false;

            int rowIndex = 0, colIndex = 0; long frameIndex = 0;

            // Get stream length
            byte[] tmp = new byte[8];
            stream.Read(tmp, 0, 8);
            String streamLengthBits = Convert.ToString(BitConverter.ToInt32(tmp, 0), 2).PadLeft(videoCapacityBits.Length, '0');

            // Add length data to video
            videoFrame = reader.ReadVideoFrame();
            fullWithVideo(ref rowIndex, ref colIndex, ref frameIndex, streamLengthBits, false);

            int textBuffer;
            String streamBits = String.Empty;
            while ((textBuffer = stream.ReadByte()) >= 0)
            {
                streamBits += Convert.ToString(textBuffer, 2).PadLeft(8, '0');
            }

            for (; frameIndex < frameCount; frameIndex++)
            {
                while (!finishHiding)
                {
                    fullWithVideo(ref rowIndex, ref colIndex, ref frameIndex, streamBits, true);
                    finishHiding = true;
                }
                writer.AddFrame(videoFrame);
                videoFrame = reader.ReadVideoFrame();
            }
            writer.Close();
            return true;
        }

        // Extract data from video
        public Stream extractFromVideo()
        {
            reader = new VideoFileReader();
            reader.Open(sourceFilePath);
            long frameCount = reader.FrameCount;

            BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());

            calculateVideoCapacity();

            int rowIndex = 0, colIndex = 0; long frameIndex = 0;

            videoFrame = reader.ReadVideoFrame();

            String textLengthBits = String.Empty;
            for (int i = 0; i < videoCapacityBits.Length; i += 3)
            {
                if (colIndex >= videoFrame.Height)
                {
                    rowIndex++;
                    colIndex = 0;
                    if (rowIndex >= videoFrame.Width)
                    {
                        rowIndex = 0;
                        frameIndex++;
                        videoFrame = reader.ReadVideoFrame();
                    }
                }

                // Get pixel color
                Color pixel = videoFrame.GetPixel(rowIndex, colIndex);
                byte redByte = pixel.R, greenByte = pixel.G, blueByte = pixel.B;
                // Get bits from colors
                textLengthBits += getLSBBit(redByte);
                if (i + 1 < videoCapacityBits.Length)
                    textLengthBits += getLSBBit(greenByte);
                if (i + 2 < videoCapacityBits.Length)
                    textLengthBits += getLSBBit(blueByte);
                // Go to next pixel
                colIndex++;
            }
            // Convert bits to number (Length of text)
            int textLength = Convert.ToInt32(textLengthBits, 2);

            if (textLength > Convert.ToInt32(videoCapacityBits, 2))
                return messageWriter.BaseStream;

            byte[] textByte = new byte[textLength];

            // Get hidden text
            for (int i = 0; i < textLength * 8; i += 8)
            {
                BitArray textBits = new BitArray(8);
                // Get a Letter (character)
                for (int k = 7; k >= 0; k -= 3)
                {
                    if (colIndex >= videoFrame.Height)
                    {
                        rowIndex++;
                        colIndex = 0;
                        if (rowIndex >= videoFrame.Width)
                        {
                            rowIndex = 0;
                            frameIndex++;
                            videoFrame = reader.ReadVideoFrame();
                        }
                    }

                    Color pixel = videoFrame.GetPixel(rowIndex, colIndex);
                    byte redByte = pixel.R, greenByte = pixel.G, blueByte = pixel.B;
                    textBits[k] = (getLSBBit(redByte) == '1');
                    textBits[k - 1] = (getLSBBit(greenByte) == '1');
                    if (k != 1)
                        textBits[k - 2] = (getLSBBit(blueByte) == '1');

                    // Go to next pixel
                    colIndex++;
                }
                messageWriter.Write(ConvertFromBitArrayToByte(textBits));
            }
            // Return the Stream
            messageWriter.Seek(0, SeekOrigin.Begin);
            return messageWriter.BaseStream;
        }
    }
}
